﻿using System.Collections.Generic;
using RPL_DataAccess_Library.Data;

namespace RPL_DataAccess_Library.Repositories {
    public class OrdersRepository : Repository<Orders> {
        private List<Orders> _list;

        protected override List<Orders> List {
            get {
                if (_list == null) {
                    _list = new List<Orders>() {
                        new Orders() { Id = "1", CurrDate = "2019.01.01", Customer = "Fairul", Total = "150000" },
                        new Orders() { Id = "2", CurrDate = "2019.01.01", Customer = "Muna", Total = "200000" },
                    };
                }
                return _list;
            }
            set {
                _list = value;
            }
        }

        public override Orders Model { get; set; }
    }
}
