﻿using System.Collections.Generic;
using RPL_DataAccess_Library.Data;

namespace RPL_DataAccess_Library.Repositories {
    public class ProductRepository : Repository<Product> {
        private List<Product> _list;

        protected override List<Product> List {
            get {
                if (_list == null) {
                    _list = new List<Product>() {
                        new Product() { Id = "1", Name = "Ayam", Price = "10000", Guarantee = "1 Hari" },
                        new Product() { Id = "2", Name = "Buaya", Price = "15000", Guarantee = "2 Hari" },
                        new Product() { Id = "3", Name = "Celeng", Price = "20000", Guarantee = "3 Hari" },
                    };
                }
                return _list;
            }
            set {
                _list = value;
            }
        }

        public override Product Model { get; set; }
    }
}
