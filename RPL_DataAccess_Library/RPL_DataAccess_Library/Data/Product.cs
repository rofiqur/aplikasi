﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPL_DataAccess_Library.Data
{
    public class Product : Item
    {
        public string Guarantee { get; set; }
    }
}
