﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Xceed.Wpf.AvalonDock.Layout;

namespace RPL_OrdersDesktop_Client.Views.Main {
    public partial class MainView : Window {
        public LayoutDocument[] PaneArray;

        public MainView() {
            InitializeComponent();
            PaneArray = new LayoutDocument[] {
                new LayoutDocument() {
                    Title = "Welcome",
                    Content = new MainWelcomeView(),
                    IsSelected = true,
                    CanFloat = false,
                },
            };
            ldp.Children.Add(PaneArray[0]);
        }

        public void TabViewControl(string title, bool flag, object content = null) {
            if (flag == true) {
                var n = PaneArray.Count() + 1;
                Array.Resize(ref PaneArray, n);
                PaneArray[n - 1] = new LayoutDocument() {
                    Content = content,
                    Title = title,
                    IsSelected = true,
                    CanClose = false,
                };
                if (content is MainWelcomeView) {
                    PaneArray[n - 1].CanClose = true;
                }
                ldp.Children.Add(PaneArray[n - 1]);
            } else {
                for (var i = 0; i < PaneArray.Count(); i++) {
                    if (PaneArray[i].Title == title) {
                        try {
                            PaneArray[i].Close();
                        } catch { }
                    }
                }
            }
        }

        private void MnuWelcome_Click(object sender, RoutedEventArgs e) {
            TabViewControl("Welcome", false);
            TabViewControl("Welcome", true, new MainWelcomeView());
        }

        private void MnuOrdersList_Click(object sender, RoutedEventArgs e) {
            TabViewControl("Orders", false);
            TabViewControl("Orders", true, new Orders.OrdersView());
        }

        private void MnuOrders_Click(object sender, RoutedEventArgs e) {

        }

        private void MnuProduct_Click(object sender, RoutedEventArgs e) {
            new Product.ProductView().ShowDialog();
        }

        private void MnuExit_Click(object sender, RoutedEventArgs e) {
            Application.Current.Shutdown();
        }
    }
}
